function drw_hud(player, prompt)
  local baseheight = TS*(#current_map+1) + 10
  function drw_bar(char, n)
    local str = ""
    for i = 1, n do
      str = str .. char
    end
    return str
  end
  function drw_info(name, value, voffset, col)
    love.graphics.setColour(255, 255, 255)
    love.graphics.print(name, TS*2, baseheight+voffset)
    love.graphics.setColour(unpack(col))
    love.graphics.print(value, TS*8, baseheight+voffset)
  end
  function drw_prompt(prompt)
    love.graphics.setFont(prompt_font) -- This kinda rhymes
    local col, spacing
    for h, str in pairs(prompt) do
      spacing = (TS/2.8)*h
      col = 100 + ((TS/2)*4)*h
      h = h - 1.5
      love.graphics.setColour(col, col, col)
      love.graphics.print(str, TS*17, baseheight+(h*(TS+6)) + spacing)
    end
    love.graphics.setFont(font)
  end
  function colour_health()
    local col = player.health/25.5
    if col > 7 then
      return {10, 255, 10}
    elseif col > 4 then
      return {255, 165, 0}
    end
    return {255, 10, 10}
  end

  drw_info("Health: ", drw_bar("█", player.health/25.5), 0,
                              colour_health())
  drw_info("Laser: ", drw_bar("█", player.laser), TS*2-10, {10, 10, 255})
  drw_info("Level: ", LEVEL, TS*4-14, {255, 255, 255})
  drw_info("Moves: ", MOVES, TS*6-24, {255, 255, 255})
  drw_prompt(prompt)
end

function drw_map(map)
  for y=1, #map do
    for x=1, #map[y] do
      love.graphics.setColour(128, 128, 128)
      if map[y][x] == ENTRANCE then
        love.graphics.setColour(100, 130, 150)
      elseif map[y][x] == WALL then
        love.graphics.setColour(168, 101, 45)
      elseif map[y][x] == HOLE then
        love.graphics.setColour(73, 45, 17)
      elseif map[y][x] == FLOOR then
        love.graphics.setColour(163, 56, 23, 100)
      end
      if x == exit.x and y == exit.y then
        love.graphics.setColour(180, 255, 150)
      end
      love.graphics.print(map[y][x], x*TS, y*TS-(TS/2))
    end
  end
end

function drw_entity(ent)
  love.graphics.setColour(ent.r, ent.g, ent.b)
  love.graphics.print(ent.c, ent.x*TS, ent.y*TS-(TS/2))
end

function drw_bolts()
  for _, b in pairs(player.bolts) do
    drw_entity(b)
  end
end
