
function gen_map(width, height)
  map = {}
  function new_map(width, height)
    for i = 1, height do
      table.insert(map, {})
    end
    for j = 1, #map do
      for i = 1, width do
        if i == 1 or i == width or j == 1 or j == #map then
          table.insert(map[j], WALL);
        else
          table.insert(map[j], FLOOR);
        end
      end
    end
  end

  new_map(43, 25)
  gen_exit(map)
  gen_player(map)

  return map
end

function gen_player(map)
  local width = #map[#map] -1
  local height = #map      -1
  local x = love.maths.random(2, width)
  local y = love.maths.random(2, height)
  local exit_dist = 10 -- player must be > 10 squares from the exit
  for j = y-(exit_dist/2), y+(exit_dist)/2 do
    for i = x-(exit_dist/2), x+(exit_dist)/2 do
      if map[y][x] == EXIT then
        gen_player(map)
        return
      end
    end
  end
  map[y][x] = ENTRANCE
  player.x = x
  player.y = y
end

function gen_exit(map)
  local width = #map[#map] -1
  local height = #map      -1
  local x = love.maths.random(2, width)
  local y = love.maths.random(2, height)
  map[y][x] = EXIT
  exit.x = x
  exit.y = y
end

function gen_laser(move_vect)
  if player.laser == 10 then
    table.insert(player.bolts, {
      c = BEAM,
      x = player.x,
      y = player.y,
      r = 100,
      g = 130,
      b = 190,
      vec = move_vect
    })
    player.laser = 0
    add_prompt(prompt, "'Zap!', goes the laser!")
  end
end

function gen_rockfall(n, map)
  function place_pillar(width, height)
    local x = love.maths.random(2, width-1)
    local y = love.maths.random(2, height-1)
    local size = love.maths.random(2, 5)
    local hole_p = love.maths.random(0, 4)

    if x == player.x and y == player.y then
      add_prompt(prompt, "A falling rock hit you! You take damage!")
      player.health = player.health - MAXHEALTH/10
    end

    if map[y][x] == WALL or map[y][x] == HOLE then
      place_pillar(width, height)
      return
    end

    for j = y, y+size do
      for i = x, x+size do
        if not (hole_p == 0) then
          map[y][x] = WALL
        else
          if exit.x == x and exit.y == y then
            map[y][x] = WALL
          else
            map[y][x] = HOLE
          end
        end
      end
    end
  end

  for i = 1, n do
    place_pillar(#map[#map], #map)
  end
end


