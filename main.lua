require 'gen'
require 'drw'

 -- Silly 'muricans
love.graphics.setColour = love.graphics.setColor
love.maths = love.math
maths = math

function bind_keys()
  local h = io.open("config.txt")
  local t = {}
  for l in h:lines() do
    table.insert(t, l)
  end
  k_left  = t[1]
  k_down  = t[2]
  k_up    = t[3]
  k_right = t[4]
  k_fire  = t[5]
  k_next  = t[6]
end
bind_keys()

WALL       = "#"
FLOOR      = "."
EXIT       = ">"
ENTRANCE   = "<"
HOLE       = "O"
BEAM       = "+"
TS         = 18
MAXHEALTH  = 255

function init()
  GAME_END_P = false
  DEATH_MESSAGE = "Do you want to restart? [Y/n]"

  DIFFICULTY = 1 -- It's more idiomatic... kind of
  LEVEL = 1
  MOVES = 0

  player = {
    r = 255,
    g = 255,
    b = 255,
    x = 6,
    y = 10,
    c = '@',
    health = MAXHEALTH,
    laser = 10,
    fire_p = false,
    bolts = {}
  }
  exit = {x = 0, y = 0}
  local seed = os.time()
  love.maths.setRandomSeed(os.time())
  current_map = gen_map()
end
function _tostring(v)
  local s = ""
  if type(v) == "string" then
      return v -- "\"" .. v .. "\""
  elseif type(v) == "table" then
    for _, e in pairs(v) do
      if _ < #v then
        s = s .. _tostring(e) .. ", "
      else
        s = s .. _tostring(e)
      end
    end
    s = "{" .. s .. "}"
  else
    s = tostring(v)
  end
  return s
end

function love.load()
  love.window.setTitle("Cave in! - finnoleary (ld33)")
  font = love.graphics.newFont(
            "font/ubuntu-font-family-0.83/UbuntuMono-B.ttf", 24)
  prompt_font = love.graphics.newFont(
            "font/ubuntu-font-family-0.83/Ubuntu-C.ttf", 24)
  love.graphics.setFont(font)

  prompt = {
    "Get to the exit (>) as fast as possible!",
    "Use " .. k_left  .. ", " ..
              k_down  .. ", " ..
              k_up    .. ", " ..
              k_right .. ", " ..
    " to move, and '" .. k_fire .. "'+dir to fire.",
    "Avoid the holes (O), and destroy/dodge the rocks (#)",
    "Press '" .. k_next ..
    "' to go down when you reach the exit. Good luck!"
  }
  phrases = {
    "The ground rumbles, throwing you about a bit!",
    "Some dust and bits of rock fall from the ceiling.",
    "You're caught off-balance as the entire structure shakes!",
    "You cough as a small plume of dust erupts around you.",
    "A deep rumble looses more rubble.",
    "The dust settles a bit, only to rush out again",
    "Yet another small sinkhole opens up",
  }
  previous_phrase = 1
  init()
end

function upd_prompt(p, no_skip)
  if not (MOVES%5 == 0) and not no_skip then
    return
  end
  for k, v in pairs(p) do
    if k+1 <= #p then
      p[k] = p[k+1]
    else
      p[k] = ""
    end
  end
end
function add_prompt(p, s)
  upd_prompt(p, true)
  p[#p] = s
end
function clear_prompt(s)
  prompt = {s, "", "", ""}
end

function chk_bounds(x, y, map)
  if not map then map = current_map end
  return ((y>1 and y<#map) and
          (x>1 and x<#map[#map]))
end
function chk_boundsplus(x, y, map)
  if not map then map = current_map end
  return ((y>2 and y<#map-1) and
          (x>2 and x<#map[#map]-1))
end
function chk_collide(x, y, tile)
  if not tile then tile = WALL end
  if not chk_bounds(x, y) then return false end
  return (current_map[y][x] == tile)
end
function player_chkhit()
  if player.health == 0 then
    add_prompt(prompt, "Ouch! Your head gets crushed by a piece of rock!")
    add_prompt(prompt, DEATH_MESSAGE)
    GAME_END_P = true
  end
end
function player_chkfall(x, y)
  if chk_collide(player.x+x, player.y+y, HOLE) then
    add_prompt(prompt, "Whoops! You fell down a hole!.")
    add_prompt(prompt, DEATH_MESSAGE)
    GAME_END_P = true
  end
end

function next_level(player)
  DIFFICULTY = DIFFICULTY + 1
  current_map = gen_map()
  player.laser = 10
  LEVEL = LEVEL + 1
end
function end_game(key)
  if key == "y" then
    init()
    clear_prompt("Great! :D")
  elseif key == "n" then
    love.event.quit()
  end
end

function add_phrase()
  local phrase_n = love.maths.random(1, 50)
  if phrase_n > 40 then
    local n = love.maths.random(1, #phrases)
    if previous_phrase == n then
      n = love.maths.random(1, #phrases)
    end
    add_prompt(prompt, phrases[n])
    previous_phrase = n
  end
end

function upd_bolt()
  for _, b in pairs(player.bolts) do
    if not chk_boundsplus(b.x, b.y) then
      table.remove(player.bolts, _)
      return
    end
    if chk_collide(b.x, b.y, WALL) then
      if b.x == exit.x and b.y == exit.y then -- Sloppy, sloppy, sloppy...
        current_map[b.y][b.x] = EXIT
      else
        current_map[b.y][b.x] = FLOOR
      end
      table.remove(player.bolts, _)
      add_prompt(prompt, "*KRUNCH*")
    end
    b.x = b.x + b.vec[1]
    b.y = b.y + b.vec[2]
  end
end

function love.keypressed(key, repeat_p)
  if key == "q" then
    love.event.quit()
  end
  if key == "r" then
    player.health = player.health - MAXHEALTH/10
  end
  player_chkhit()

  upd_bolt() -- Wouldn't it be cool if the bolt carried on going? :D
  if GAME_END_P then
    end_game(key)
    return
  else
    MOVES = MOVES + 1
    add_phrase()
    upd_prompt(prompt)
  end

  local move_vectors = {{-1, 0}, {0, 1}, {0, -1}, {1, 0}}
  if key == "." and current_map[player.y][player.x] == EXIT then
    next_level(player)
  end
  if player_fire(key, player, move_vectors) then return end
  gen_rockfall(DIFFICULTY, current_map)
  player_move(key, player, move_vectors)
end

function player_fire(key, player, move_vectors)
  if key == k_fire then
    player.fire_p = true
    return true
  elseif player.fire_p == true then
        if key == k_left  then gen_laser(move_vectors[1])
    elseif key == k_down  then gen_laser(move_vectors[2])
    elseif key == k_up    then gen_laser(move_vectors[3])
    elseif key == k_right then gen_laser(move_vectors[4])
    else
      add_prompt(prompt, "Unable to comply: That's not a direction!")
    end
    player.fire_p = false
    return true
  end
end

function player_move(key, player, move_vectors)
  if key == k_left or key == k_down or key == k_up or key == k_right then
    local v
    if key == k_left then v = move_vectors[1] end
    if key == k_down then v = move_vectors[2] end
    if key == k_up then v = move_vectors[3] end
    if key == k_right then v = move_vectors[4] end

    if player_chkfall(v[1], v[2]) then return end
    if not chk_collide(player.x+v[1], player.y+v[2]) then
      player.x = player.x + v[1]
      player.y = player.y + v[2]
    end
  elseif key == "space" then
    gen_rockfall(DIFFICULTY, current_map)
  end
end

function love.draw()
  drw_map(current_map)
  drw_entity(player)
  drw_bolts()
  drw_hud(player, prompt)
end

